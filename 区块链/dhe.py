def judge_prime(number):
    """
    判断是否为质数，输入大于1的整数
    :param number: 数字
    :return: 判断结果1不是质数，0是质数
    """
    status = 0
    if number < 1 or type(number) != int:
        return '格式不对'
    else:
        for i in range(2, number):
            if number % i == 0:
                status = 1
        return status


def isPrime(a, b):
    """
    验证是否为互质
    :param a: 除数
    :param b: 被除数
    :return: 判断结果1为互质，0不是
    """
    while b != 0:
        temp = b
        b = a % b
        a = temp
    if a == 1:
        return 1
    else:
        return 0


def find_prine(number):
    """
    找出该数的本原根
    :param number: 数
    :return: 本原根
    """
    primeList = []
    for i in range(1, number + 1):
        ans = isPrime(i, number)
        if ans == 1:
            primeList.append(i)
    byg = []
    prime_temp = []
    for j in primeList:
        for i in range(1, len(primeList) + 1):
            prime_temp.append(j ** i % number)
        prime_temp.sort()
        if primeList == prime_temp:
            byg.append(j)
        else:
            pass
        prime_temp = []
    return byg


def calculation(number, p, g):
    return pow(g, number) % p


if __name__ == '__main__':
    p = 109
    g = 6
    public_a = '0xD9C51bEF410960540E059054AC2258FE59101eCc'
    public_b = '0xa14dDA6759C4aB71392626bb01D62fd0b96C3d19'
    # alice
    private_a = '0xabffaaedaafe151eaa778c7429893634aedeb8692bef44fab025229908e6675f'
    number_a = 15
    first_number_a = calculation(number_a, p, g)
    first_sign_a = message_sign(str(first_number_a), private_a)
    result = verifity(str(first_number_b), first_sign_b, public_b)
    if result == '验证一致':
        key = calculation(number_a, p, first_number_b)
        print(key)
    result = verifity(str(first_number_a), first_sign_a, public_a)
    if result == '验证一致':
        key = calculation(number_b, p, first_number_a)
        print(key)
