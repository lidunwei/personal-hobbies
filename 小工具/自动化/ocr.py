from paddleocr import PaddleOCR


def baidu_ocr(image_path):
    """
    百度ocr调用
    :param image_path:图片路径 
    :return: 识别结果包含位置
    """
    ocr = PaddleOCR()
    img_path = image_path
    result = ocr.ocr(img_path)
    return result
