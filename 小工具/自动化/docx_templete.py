from mailmerge import MailMerge

template = "templates/name.docx"
document_1 = MailMerge(template)
print("Fields included in {}: {}".format(template, document_1.get_merge_fields()))
document_1.merge(
    name=u'勒布朗',
    number='123456789',
    true_number='2018',
    pub_key='7',
    prv_key=u'洛杉矶湖人',
    owner=u'联盟第一人',
    time = u'2012年12月12日'

)
document_1.write('test666.docx')