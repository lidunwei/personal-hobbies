def encode(string):
    string_len = len(string)
    i = 0
    result = []
    while i < string_len:
        count = 1
        c = string[i]
        i += 1
        while i < string_len and string[i] == c:
            count += 1
            i += 1
        if count == 1:
            result.append(c)
        else:
            result += [c, c, count]
    return result
print(encode('111011100100000010'))
# 111011100100000010
# 200233113103