def to_array(x):
    List = []
    x =int(x)
    for i in range(4):
        a = x % 10
        List.append(a)
        x = int(x / 10)
    return List


def to_number(list):
    num = 0
    for i in range(4):
        num = num * 10 + list[i]
    return num


wrong_list = [1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999, 0000]
while True:
    x = input('请输入一个四位不完全相同的四位数：')

    try:
        int(x)
    except:
        print('输入好像错误了吧~~~\n要求：\n(1)一定是纯数字；\n(2)一定是四位数；\n(3)不可以是所有数字相同的四位数。')
        continue
    else:
        int(x)
        if x in wrong_list:
            print('输入好像错误了吧~~~\n要求：\n(1)一定是纯数字；\n(2)一定是四位数；\n(3)不可以是所有数字相同的四位数。')
            continue
        else:
            while True:
                list = to_array(x)
                list.sort()
                min = to_number(list)
                list.sort(reverse=True)
                max = to_number(list)
                x = max - min
                print(max, '-', min, '=', x)
                if (x == 0 or x == 6174):
                    break
