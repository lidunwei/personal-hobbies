# 把mysql数据库中的数据导入mongodb中
import pymysql
import pymongo

# 创建mysql的数据库连接
con = pymysql.connect(host='****', port=3306, user='****', password='****', db='****')
# 获取游标
cur = con.cursor(cursor=pymysql.cursors.DictCursor)
# 查询student表
try:
  cur.execute('select * from disease_knowledge')
  client = pymongo.MongoClient(host='****', port=27017)
  db = client['disease_knowledge']
  for row in cur.fetchall():
      print(row)
      db.student.insert_one(row)
except Exception as e:
  print(e)
finally:
  con.close()
  client.close()