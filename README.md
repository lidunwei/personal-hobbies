# 个人爱好

#### 介绍
一些简单的个人爱好、及实用工具分享

#### 软件架构
1.  区块链
     - 默克尔树python实现
     - dhe密钥协商算法python实现
2.  小工具
     - 数据库
       - mysql转-mongodb
     - 爬虫
       - 谷歌热度爬虫
     - 自动化
       - word模板批量生成
       - 百度轻量级ocr识别
       - pdf拆分

#### 安装教程

1.  pip installl -r requirements.txt

#### 参与贡献

1.  联系13564180096

